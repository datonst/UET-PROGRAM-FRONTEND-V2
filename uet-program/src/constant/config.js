
const apiKey = process.env.REACT_APP_BACKEND_API_KEY;

export const baseURL = () => {
    const apiUrl = "backend.stunet.site";
    // console.log(apiUrl)
    // console.log(apiPort)
    // const apiUrl = "localhost";
    // const apiPort = "8080";
    return `https://${apiUrl}/api-client/v1`;
}